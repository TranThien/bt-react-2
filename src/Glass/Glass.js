import React, { Component } from "react";
import { listGlasses } from "./dataGlass";
import DetailGlass from "./DetailGlass";
import RenderAvatar from "./RenderAvatar";
import RenderListGlass from "./RenderListGlass";
import styles from "./style.module.css";

export default class Glass extends Component {
  state = {
    dataGlass: listGlasses,
    decs: {},
  };
  renderGlass = () => {
    return this.state.dataGlass.map((item) => {
      return (
        <RenderListGlass
          handleClick={this.handleClickDecsGlass}
          data={item}
          key={item.id.toString()}
        />
      );
    });
  };
  // hàm state ở đâu thì viết setState ở đó
  handleClickDecsGlass = (glass) => {
    this.setState({
      decs: glass,
    });
  };
  render() {
    return (
      <div className="container">
        <div className={styles.model}>
          <div>
            <RenderAvatar />
          </div>
          <div className={styles.detail}>
            <DetailGlass detail={this.state.decs} />
          </div>
        </div>
        <div className="row">{this.renderGlass()}</div>
      </div>
    );
  }
}
