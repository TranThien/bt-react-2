import React, { Component } from "react";

export default class RenderListGlass extends Component {
  render() {
    const { url } = this.props.data;
    return (
      <div
        className="col-6 p-3"
        onClick={() => {
          this.props.handleClick(this.props.data);
        }}
      >
        <img src={url} alt="" />
      </div>
    );
  }
}
