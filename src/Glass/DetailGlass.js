import React, { Component } from "react";
import styles from "./style.module.css";

export default class extends Component {
  render() {
    const { name, price, desc, url } = this.props.detail;
    console.log(this.props.detail);
    return (
      <div>
        <div className={styles.img}>
          <img style={{ height: "180px" }} src={url} alt="" />
        </div>
        <div className={styles.desc}>
          <h3 className="text-danger">{name}</h3>
          <h2 className="text-danger">{price}</h2>
          <h4 className="text-danger">{desc}</h4>
        </div>
      </div>
    );
  }
}
