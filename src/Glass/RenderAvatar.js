import React, { Component } from "react";
import avatar from "../glassesImage/model.jpg";

export default class RenderAvatar extends Component {
  render() {
    return (
      <div className="d-block">
        <img style={{ width: "100%", height: "100%" }} src={avatar} alt="" />
      </div>
    );
  }
}
